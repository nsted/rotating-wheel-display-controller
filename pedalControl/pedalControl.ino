#include "DualVNH5019MotorShield.h"

#define VEL_PIN        A2
#define BRAKE_PIN      A3
#define ENCODER_PIN    A4
#define CLOCKWISE      1
#define COUNTER_CLOCKWISE   -1

DualVNH5019MotorShield md;

const int maxVel = 150;
const int maxBrake = 600;
const int cutoff = 10;
int lastChargeState = 0;
int delayval = 25;


void setup() {
  Serial.begin(115200);
  md.init();
}

void loop() {
  int vel = readVel();
  int brake = readBrake();
  vel *= CLOCKWISE;
  
  md.setM1Speed(vel);
  if(brake) {
    md.setM1Brake(brake);
  }
  stopIfFault();

  double acceleration = readAcceleration();
  int chargeState = calcChargeStatus( acceleration, vel, brake );

  if(chargeState != lastChargeState) {
    Serial.println(chargeState);
  }
  lastChargeState = chargeState;

  delay(delayval);

//  Serial.print("speed: ");
//  Serial.print(vel);
//  Serial.print(", brake: ");
//  Serial.print(brake);
////  Serial.print(", mA: ");
////  Serial.print(md.getM1CurrentMilliamps());    
//  Serial.print(", acceleration: ");
//  Serial.print(acceleration);
//  Serial.print(", chargeState: ");
//  Serial.println(chargeState);
}


int calcChargeStatus(double acceleration, int vel, int brake) {
  const int numReadings = 10;
  static int readings[numReadings] = {0};      // the readings from the analog input
  static int readIndex = 0;                   // the index of the current reading
  static int total = 0;                       // the running total
  static int average = 0;                // the average
  
  int chargeState = 0;

  // if accelerating, draw a charge
  if (vel > 30) {
    chargeState = 1;
  } 
  // if coasting, load a charge
  else if ((acceleration < -0.0) && (brake < 10)) {
    chargeState = -1;
  } 
  // if braking, load a charge at twice the rate
  else if (acceleration < -0.0 && brake > 10) {
    chargeState = -2;
  }   
  // otherwise no consumption
  else {
    chargeState = 0;
  }  

  total -= readings[readIndex];
  readings[readIndex] = chargeState;
  total += readings[readIndex];
  readIndex++;
  if (readIndex >= numReadings) {
    readIndex = 0;
  }
  average = total / numReadings;
  
  return average;
}

// read from and constrain the vel pedal
int readVel() {
  int sense = analogRead(VEL_PIN);
  sense = map(sense, 160, 860, 0, maxVel);
  sense = constrain(sense, 0, maxVel);
  if(sense < cutoff) {
    sense = 0;
  }  
  return sense;
}

// read from and constrain the brake pedal
int readBrake() {
  int sense = analogRead(BRAKE_PIN);
  sense = map(sense, 120, 710, 0, maxBrake);
  sense = constrain(sense, 0, maxBrake);
  if(sense < cutoff) {
    sense = 0;
  }
  return sense;
}

// stop that motor if there's a problem!
void stopIfFault()
{
  if (md.getM1Fault())
  {
    Serial.println("M1 fault");
    while (1);
  }
  if (md.getM2Fault())
  {
    Serial.println("M2 fault");
    while (1);
  }
}

// read the acceleration, calculated from encoder feedback
double readAcceleration() {
  static long timeStamp;
  const int limit = 10;
  static int angle;
  static double rpm;

  // read the current angle
  int lastAngle = angle;
  angle = analogRead(ENCODER_PIN);
  angle = map(angle, 0, 1023, 0, 360);
  angle = constrain(angle, 0, 360);

  // adjust if there's been a carry over
  int angleDiff = angle - lastAngle;
  if (abs(angleDiff) > 180) {
    if (angleDiff > 0) {
      angleDiff -= 360;
    }
    else {
      angleDiff += 360;
    }
  }

  // how long was interval since last sample? And how many intervals in a minute?
  int timeDiff = (millis() - timeStamp);
  timeStamp = millis();
  int timeDivs = 60000 / timeDiff;

  // calc the rpm and acceleration values
  int lastRpm = rpm;  
  rpm = calcRpm(angleDiff, timeDiff);
  double acceleration = calcAcceleration(rpm, timeDiff);
  acceleration = int(acceleration * 100) / 100.0;

//  Serial.print(rpm);
//  Serial.print(",");
//  Serial.print(acceleration,6);
//  Serial.print(",");
//  Serial.println(3.0);

  return acceleration;
}

// calculate RPM using a moving average
int calcRpm(int angleDiff, int timeDiff) {
  const int numReadings = 10;
  static int readings[numReadings] = {0};      // the readings from the analog input
  static int readIndex = 0;                 // the index of the current reading
  static int total = 0;                  // the running total
  static int average = 0;                // the average

  int rpm = ((abs(angleDiff) / 360.0) / timeDiff) * 60000.0;   // (dA / dT)

  total -= readings[readIndex];
  readings[readIndex] = rpm;
  total += readings[readIndex];
  readIndex++;
  if (readIndex >= numReadings) {
    readIndex = 0;
  }
  average = total / numReadings;

  const int noiseThresh = 8;
  if(average < noiseThresh) {
    average = 0;
  }

  return average;
}

// calculate acceleration using a moving average
double calcAcceleration(int rpm, int timeDiff) {
  const int numReadings = 30;
  static int readings[numReadings] = {0};      // the readings from the analog input
  static int readIndex = 0;                   // the index of the current reading
  static int total = 0;                       // the running total
  static double average = 0.0;                // the average
  static int lastRpm = 0;

  int acceleration = ((rpm - lastRpm) / (timeDiff * 1.0)) * 1000;
  lastRpm = rpm;
  
  total -= readings[readIndex];
  readings[readIndex] = acceleration;
  total += readings[readIndex];
  
  readIndex++;
  if (readIndex >= numReadings) {
    readIndex = 0;
  }
  average = (total / numReadings) / 1000.0; 

  return average;
}
